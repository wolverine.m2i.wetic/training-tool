# DOC ANGULAR PAR DEFAUT
----------------------

# TrainingTool

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

#DOC PERSONNALISEE
----------------------

## Dépendances

1- nodejs : v16.14.1
2- npm : 8.5.0
3- angular-cli : 13.3.0 (voir package.json)
4- bootstrap 4, jquery 3.6.0, popper.js 1.16.1

## Initialisation du projet
- npm install
- ng serve

## ajout de nouveau groupe 
1- generer le component
```
ng g c <nom du groupe>
```
2- declaler le routing dans app-routing.module.ts

## ajout de nouveau sous groupe
1- generer le component
```
ng g c <nom du groupe>/<nom du sous groupe>
```
2- declaler le routing dans app-routing.module.ts
