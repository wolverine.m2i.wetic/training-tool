import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pc-information',
  templateUrl: './pc-information.component.html',
  styleUrls: ['./pc-information.component.css']
})
export class PcInformationComponent implements OnInit {
  componentTitle = "PC Information";

  constructor() { }

  ngOnInit(): void {
  }

}
