import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { TrainingToolsComponent } from './training-tools/training-tools.component';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { PcInformationComponent } from './pc-information/pc-information.component';
import { CatalinaComponent } from './configuration/catalina/catalina.component';
import { BigsurComponent } from './configuration/bigsur/bigsur.component';

@NgModule({
  declarations: [
    AppComponent,
    CatalinaComponent,
    BigsurComponent,
    HomeComponent,
    ConfigurationComponent,
    TrainingToolsComponent,
    OnboardingComponent,
    WarehouseComponent,
    PcInformationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
