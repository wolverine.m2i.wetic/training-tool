import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalinaComponent } from './catalina.component';

describe('CatalinaComponent', () => {
  let component: CatalinaComponent;
  let fixture: ComponentFixture<CatalinaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatalinaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
