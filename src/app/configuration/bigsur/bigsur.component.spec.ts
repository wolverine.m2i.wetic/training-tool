import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BigsurComponent } from './bigsur.component';

describe('BigsurComponent', () => {
  let component: BigsurComponent;
  let fixture: ComponentFixture<BigsurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BigsurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BigsurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
