import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-training-tools',
  templateUrl: './training-tools.component.html',
  styleUrls: ['./training-tools.component.css']
})
export class TrainingToolsComponent implements OnInit {

  componentTitle = "Training tools";

  constructor() { }

  ngOnInit(): void {
  }

}
