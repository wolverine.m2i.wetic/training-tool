import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingToolsComponent } from './training-tools.component';

describe('TrainingToolsComponent', () => {
  let component: TrainingToolsComponent;
  let fixture: ComponentFixture<TrainingToolsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainingToolsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
