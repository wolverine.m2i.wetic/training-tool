import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BigsurComponent } from './configuration/bigsur/bigsur.component';
import { CatalinaComponent } from './configuration/catalina/catalina.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { HomeComponent } from './home/home.component';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { PcInformationComponent } from './pc-information/pc-information.component';
import { TrainingToolsComponent } from './training-tools/training-tools.component';
import { WarehouseComponent } from './warehouse/warehouse.component';

const routes: Routes = [
  {path:'', redirectTo:'home', pathMatch:'full'},
  {path: 'home', component:HomeComponent},
  {path:'configuration',
      children:[
        {path:'', component:ConfigurationComponent},
        {path: 'catalina', component:CatalinaComponent},  
        {path: 'bigsur', component:BigsurComponent}  
      ]
  },
  {path:'training-tools',
    children:[
      {path:'', component:TrainingToolsComponent},
    ]
  },
  {path:'onboarding',
    children:[
      {path:'', component:OnboardingComponent},
    ]
  },
  {path:'warehouse',
    children:[
      {path:'', component:WarehouseComponent},
    ]
  },
  {path:'pc-information',
    children:[
      {path:'', component:PcInformationComponent},
    ]
  }
  // {path:'**', component:PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
